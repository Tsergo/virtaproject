package com.pocket.repository;

import com.pocket.data.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query(value = "select id from company, " +
            "(select @pv \\:= :companyId) initialisation " +
            "where  find_in_set(parent_company_id, @pv) > 0 " +
            "and    @pv \\:= concat(@pv, ',', id)", nativeQuery = true)
    Set<Long> findAllByCompanyId(Long companyId);
}

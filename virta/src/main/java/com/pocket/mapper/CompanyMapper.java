package com.pocket.mapper;

import com.pocket.data.Company;
import com.pocket.dto.CompanyDTO;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CompanyMapper {

    List<CompanyDTO> companyDTOListFromCompanyList(List<Company> companyList);

    @Mappings({})
    Company companyFromCompanyDTO(CompanyDTO companyDTO);

    @Mappings({
            @Mapping(source = "parent.id", target = "parentId")
    })
    CompanyDTO companyDTOFromCompany(Company company);

    @Mappings({
            @Mapping(target = "id", ignore=true)
    })
    Company getUpdateCompanyFromCompanyDTO(CompanyDTO companyDTO, @MappingTarget Company company);

}

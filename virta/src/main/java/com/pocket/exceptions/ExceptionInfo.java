package com.pocket.exceptions;

public enum ExceptionInfo {
	ENTITY_NOT_FOUND("Entity with given parameters not found", 1),
	INVALID_PARAMETER("Invalid Parameters", 2);

	private final int code;
	private final String message;

	ExceptionInfo(String message, int code) {
		this.code = code;
		this.message = message;
	}

	public int code() {
		return code;
	}

	public String message() {
		return message;
	}
}

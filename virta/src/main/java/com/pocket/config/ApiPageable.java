package com.pocket.config;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({
	ElementType.METHOD,
	ElementType.ANNOTATION_TYPE,
	ElementType.TYPE
})
@Retention(RetentionPolicy.RUNTIME)
@ApiImplicitParams({
	@ApiImplicitParam(
		name = "page",
		dataType = "Integer",
		paramType = "query",
		value = "Page you want to retrieve. Defaults is 0."),
	@ApiImplicitParam(
		name = "size",
		dataType = "Integer",
		paramType = "query",
		value = "Size of the page you want to retrieve. Defaults is 20."),
	@ApiImplicitParam(
		name = "sort",
		allowMultiple = true,
		dataType = "string",
		paramType = "query",
		value = "Properties that should be sorted by in the format property,property(,ASC|DESC). Default sort direction is ascending. Use multiple sort parameters if you want to switch directions for example, ?sort=name,asc."
	)
})
public @interface ApiPageable {
}

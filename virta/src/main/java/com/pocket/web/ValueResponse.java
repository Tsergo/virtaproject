package com.pocket.web;

public class ValueResponse<T> {

	private T value;

	public ValueResponse() {
	}

	public ValueResponse(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

}

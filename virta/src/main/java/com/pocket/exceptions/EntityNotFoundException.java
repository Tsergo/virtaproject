package com.pocket.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public final class EntityNotFoundException extends CustomException {

	public EntityNotFoundException() {
		super(ExceptionInfo.ENTITY_NOT_FOUND);
	}

	public EntityNotFoundException(String message) {
		super(message, ExceptionInfo.ENTITY_NOT_FOUND);
	}

}

package com.pocket.controller;

import com.pocket.config.ApiPageable;
import com.pocket.dto.StationDTO;
import com.pocket.service.StationService;
import com.pocket.web.PageableDTO;
import com.pocket.web.ValueResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value = "/api/stations")
public class StationController {

    private final StationService stationService;

    public StationController(StationService stationService){
        this.stationService = stationService;
    }

    @GetMapping
    @ApiPageable
    public ResponseEntity<PageableDTO<StationDTO>> list(@ApiIgnore Pageable pageable){
        return ResponseEntity.ok(stationService.findAll(pageable));
    }

    @GetMapping("/within")
    @ApiPageable
    public ResponseEntity<PageableDTO<StationDTO>> listWithinRadius(@RequestParam Integer radius,
                                                                    @RequestParam Double startLat,
                                                                    @RequestParam Double startLng,
                                                                    @ApiIgnore Pageable pageable
    ){
        return ResponseEntity.ok(stationService.findAllWithinRadius(radius, startLat, startLng, pageable));
    }

    @GetMapping("/company/{companyId}")
    @ApiPageable
    public ResponseEntity<PageableDTO<StationDTO>> listByCompanyId(@PathVariable Long companyId, @ApiIgnore Pageable pageable){
        return ResponseEntity.ok(stationService.findAllByCompanyId(companyId, pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<StationDTO> get(@PathVariable Long id) {
        return ResponseEntity.ok(stationService.findOne(id));
    }

    @PostMapping
    public ResponseEntity<StationDTO> create(@RequestBody StationDTO stationDTO){
        return ResponseEntity.ok(stationService.create(stationDTO));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<StationDTO> update(@PathVariable Long id, @RequestBody StationDTO stationDTO){
        return ResponseEntity.ok(stationService.update(id, stationDTO));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ValueResponse<Boolean>> delete(@PathVariable Long id){
        return ResponseEntity.ok(stationService.delete(id));
    }

}

package com.pocket.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidParametersException extends CustomException {

	public InvalidParametersException() {
		super(ExceptionInfo.INVALID_PARAMETER);
	}

	public InvalidParametersException(String message) {
		super(message, ExceptionInfo.INVALID_PARAMETER);
	}

}

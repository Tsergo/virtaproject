package com.pocket.controller;

import com.pocket.config.ApiPageable;
import com.pocket.dto.CompanyDTO;
import com.pocket.service.CompanyService;
import com.pocket.web.PageableDTO;
import com.pocket.web.ValueResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping(value = "/api/companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService){
        this.companyService = companyService;
    }

    @GetMapping
    @ApiPageable
    public ResponseEntity<PageableDTO<CompanyDTO>> list(@ApiIgnore Pageable pageable){
        return ResponseEntity.ok(companyService.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompanyDTO> get(@PathVariable Long id) {
        return ResponseEntity.ok(companyService.findOne(id));
    }

    @PostMapping
    public ResponseEntity<CompanyDTO> create(@RequestBody CompanyDTO companyDTO){
        return ResponseEntity.ok(companyService.create(companyDTO));
    }

    @PutMapping(value = "{id}")
    public ResponseEntity<CompanyDTO> update(@PathVariable Long id, @RequestBody CompanyDTO companyDTO){
        return ResponseEntity.ok(companyService.update(id, companyDTO));
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity<ValueResponse<Boolean>> delete(@PathVariable Long id){
        return ResponseEntity.ok(companyService.delete(id));
    }
}

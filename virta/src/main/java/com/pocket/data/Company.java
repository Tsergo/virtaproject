package com.pocket.data;

import javax.persistence.*;

@Entity
public class Company extends AbstractEntity{

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "parent_company_id",
            foreignKey = @ForeignKey(name = "fk_parent_company_id")

    )
    protected Company parent;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getParent() {
        return parent;
    }

    public void setParent(Company parent) {
        this.parent = parent;
    }

}


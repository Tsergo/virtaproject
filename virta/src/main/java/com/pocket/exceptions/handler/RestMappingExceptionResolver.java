package com.pocket.exceptions.handler;

import com.pocket.exceptions.CustomException;
import com.pocket.exceptions.InvalidParametersException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

public class RestMappingExceptionResolver extends AbstractHandlerExceptionResolver {

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonConverter;


	@Override
	protected ModelAndView doResolveException(HttpServletRequest request,
	                                          HttpServletResponse response,
	                                          Object handler, Exception ex
	) {
		logger.error(ex.getMessage(), ex);
		HttpStatus status = determineStatusCode(ex);
		return getModelAndView(request, ex, status);
	}

	private HttpStatus determineStatusCode(Exception ex) {
		ResponseStatus responseStatusAnnotation = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
		if (responseStatusAnnotation != null) {
			return responseStatusAnnotation.value();
		}
		return HttpStatus.INTERNAL_SERVER_ERROR;
	}

	private ModelAndView getModelAndView(HttpServletRequest request, Exception ex, HttpStatus status) {
		MappingJackson2JsonView view = new MappingJackson2JsonView(jacksonConverter.getObjectMapper());
		view.setExtractValueFromSingleKeyModel(true);
		ModelAndView mv = new ModelAndView(view);
		RestExceptionResponse response;
		if (ex instanceof MethodArgumentNotValidException) {
			MethodArgumentNotValidException exception = (MethodArgumentNotValidException) ex;
			var message = exception.getBindingResult().getAllErrors().get(0);
			var e = new InvalidParametersException(message.getDefaultMessage());
			response = getResponseObject(request, e, status);
		} else {
			response = getResponseObject(request, ex, status);
		}
		mv.addObject(response);
		mv.setStatus(status);
		return mv;
	}

	private RestExceptionResponse getResponseObject(HttpServletRequest request, Exception ex, HttpStatus status) {
		RestExceptionResponse response = new RestExceptionResponse();
		ex = ex instanceof IllegalArgumentException ? new InvalidParametersException(ex.getMessage()) : ex;
		if (ex instanceof CustomException) {
			final int code = ((CustomException) ex).getCode();
			response.setCode(code);
		}
		response.getMessages().add(ex.getMessage());
		response.setStatus(status.value());
		response.setError(ex.getClass().getSimpleName());
		response.setPath(request.getServletPath());
		response.setTimestamp(LocalDateTime.now());
		return response;
	}

}

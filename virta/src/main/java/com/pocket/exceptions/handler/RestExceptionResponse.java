package com.pocket.exceptions.handler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class RestExceptionResponse {

	private int status;
	private int code;
	private String path;
	private String error;
	private LocalDateTime timestamp;
	private List<String> messages;

	public RestExceptionResponse() {
		this.messages = new ArrayList<>();
	}

	public RestExceptionResponse(int status,
	                             String path,
	                             String error,
	                             LocalDateTime timestamp,
	                             List<String> messages) {
		this.status = status;
		this.path = path;
		this.error = error;
		this.timestamp = timestamp;
		this.messages = messages;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
}

package com.pocket.controller;

import com.google.gson.reflect.TypeToken;
import com.pocket.common.AbstractSpringRunnerTest;
import com.pocket.data.Company;
import com.pocket.dto.CompanyDTO;
import com.pocket.repository.CompanyRepository;
import com.pocket.web.PageableDTO;
import com.pocket.web.ValueResponse;
import io.restassured.http.ContentType;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

public class CompanyControllerTest extends AbstractSpringRunnerTest {

    private static final String CONTROLLER_PATH = "/api/companies";

    private List<Company> companyList = new ArrayList<>();

    @Autowired
    private CompanyRepository companyRepository;

    @Before
    public void initData() {
        Company firstCompany = new Company();
        firstCompany.setName("company" + RandomStringUtils.randomAlphanumeric(2));
        companyRepository.save(firstCompany);

        Company secondCompany = new Company();
        secondCompany.setName("company" + RandomStringUtils.randomAlphanumeric(2));
        companyRepository.save(secondCompany);

        Company thirdCompany = new Company();
        thirdCompany.setName("company" + RandomStringUtils.randomAlphanumeric(2));
        companyRepository.save(thirdCompany);

        companyList.add(firstCompany);
        companyList.add(secondCompany);
        companyList.add(thirdCompany);
    }

    @After
    public void clearData() {
        companyRepository.deleteAll();
    }

    @Test
    public void testGetCompanies() {

        PageableDTO<CompanyDTO> response = given()
                .contentType(ContentType.JSON)
                .param("size",2)
                .param("sort","id,desc")
            .when()
                .get(CONTROLLER_PATH)
            .then()
                .statusCode(HttpStatus.SC_OK)
                .extract().as(new TypeToken<PageableDTO<CompanyDTO>>(){}.getType());

        assertEquals(0, response.getPage());
        assertEquals(2, response.getSize());
        assertEquals(2, response.getTotalPages());

        companiesEqualsToCompanyDTOs(Arrays.asList(companyList.get(2), companyList.get(1)), response.getItems());
    }

    @Test
    public void testGetCompany() {
        Company company = companyList.get(0);

        CompanyDTO response = given()
                .contentType(ContentType.JSON)
           .when()
                .get(CONTROLLER_PATH + "/{id}", company.getId())
           .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(ContentType.JSON)
                .extract().as(CompanyDTO.class);

        companyEqualsToCompanyDTO(company, response);
    }

    @Test
    public void testCreateCompany() {
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setName("someName" + RandomStringUtils.randomAlphanumeric(2));

        CompanyDTO response = given()
                .contentType(ContentType.JSON)
                .body(companyDTO)
            .when()
                .post(CONTROLLER_PATH)
            .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(ContentType.JSON)
                .extract().as(CompanyDTO.class);

        List<Company> companies = companyRepository.findAll();
        assertEquals(companyList.size() + 1, companies.size());
        companyEqualsToCompanyDTO(companies.get(companyList.size()), response);
    }

    @Test
    public void testUpdateCompany() {
        Company company = companyList.get(0);

        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setName("abc " + RandomStringUtils.randomAlphanumeric(2));

        CompanyDTO response = given()
                .contentType(ContentType.JSON)
                .body(companyDTO)
           .when()
                .put(CONTROLLER_PATH + "/{id}", company.getId())
           .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(ContentType.JSON)
                .extract().as(CompanyDTO.class);

        Company expectedCompany = companyRepository.getOne(company.getId());
        companyEqualsToCompanyDTO(expectedCompany, response);
    }

    @Test
    public void testDeleteCompany() {
        Company company = companyList.get(0);

        ValueResponse<Boolean> response = given()
                .contentType(ContentType.JSON)
           .when()
                .delete(CONTROLLER_PATH + "/{id}", company.getId())
           .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(ContentType.JSON)
                .extract().as(new TypeToken<ValueResponse<Boolean>>(){}.getType());

        assertEquals(true, response.getValue());
        boolean isExists = companyRepository.existsById(company.getId());
        assertFalse(isExists);
    }

    private void companiesEqualsToCompanyDTOs(List<Company> companyList, List<CompanyDTO> companyDTOList){
        Assert.assertEquals(companyList.size(), companyDTOList.size());
        for (int i = 0;i < companyList.size();i++) {
            companyEqualsToCompanyDTO(companyList.get(i), companyDTOList.get(i));
        }

    }
    private void companyEqualsToCompanyDTO(Company company, CompanyDTO companyDTO) {
        assertEquals(company.getId(), companyDTO.getId());
        assertEquals(company.getName(), companyDTO.getName());
        if (company.getParent() != null) {
            assertEquals(company.getParent().getId(), companyDTO.getParentId());
        } else {
            assertNull(companyDTO.getParentId());
        }
    }
}

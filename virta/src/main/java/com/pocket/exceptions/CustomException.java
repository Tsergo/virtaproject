package com.pocket.exceptions;

public class CustomException extends RuntimeException {

	private final int code;

	protected CustomException(ExceptionInfo exceptionInfo) {
		super(exceptionInfo.message());
		this.code = exceptionInfo.code();
	}

	protected CustomException(String message, ExceptionInfo exceptionInfo) {
		super(message);
		this.code = exceptionInfo.code();
	}

	protected CustomException(String message, ExceptionInfo exceptionInfo, Throwable cause) {
		super(message, cause);
		this.code = exceptionInfo.code();

	}

	public int getCode() {
		return code;
	}

}

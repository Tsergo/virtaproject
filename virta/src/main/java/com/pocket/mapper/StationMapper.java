package com.pocket.mapper;

import com.pocket.data.Station;
import com.pocket.dto.StationDTO;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StationMapper {

    List<StationDTO> stationDTOListFromStationList(List<Station> stationList);

    @Mappings({})
    Station stationFromStationDTO(StationDTO stationDTO);

    @Mappings({
            @Mapping(source = "company", target = "companyDTO")
    })
    StationDTO stationDTOFromStation(Station station);

    @Mappings({
            @Mapping(target = "id", ignore=true)
    })
    Station getUpdateStationFromStationDTO(StationDTO stationDTO, @MappingTarget Station station);

}

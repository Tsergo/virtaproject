package com.pocket.service;

import com.pocket.data.Company;
import com.pocket.data.Station;
import com.pocket.dto.CompanyDTO;
import com.pocket.dto.StationDTO;
import com.pocket.exceptions.EntityNotFoundException;
import com.pocket.exceptions.InvalidParametersException;
import com.pocket.mapper.StationMapper;
import com.pocket.repository.CompanyRepository;
import com.pocket.repository.StationRepository;
import com.pocket.web.PageableDTO;
import com.pocket.web.ValueResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class StationService {

    private final StationRepository stationRepository;

    private final StationMapper stationMapper;

    private final CompanyRepository companyRepository;

    public StationService(StationRepository stationRepository, StationMapper stationMapper,
                          CompanyRepository companyRepository){
        this.stationRepository = stationRepository;
        this.stationMapper = stationMapper;
        this.companyRepository = companyRepository;
    }

    public PageableDTO<StationDTO> findAll(Pageable pageable){
        Page<Station> stationPage = stationRepository.findAll(pageable);
        return new PageableDTO<>(stationPage, stationMapper.stationDTOListFromStationList(stationPage.getContent()));
    }

    public PageableDTO<StationDTO> findAllWithinRadius(Integer radius, Double startLatitude, Double startLongitude, Pageable pageable){
        Page<Station> stationPage = stationRepository.findAllWithinRadius(radius, startLatitude, startLongitude, pageable);
        return new PageableDTO<>(stationPage, stationMapper.stationDTOListFromStationList(stationPage.getContent()));
    }

    public PageableDTO<StationDTO> findAllByCompanyId(Long companyId, Pageable pageable){
        Set<Long> companyIds = companyRepository.findAllByCompanyId(companyId);
        companyIds.add(companyId);

        Page<Station> stationPage = stationRepository.findAllByCompanyId(companyIds, pageable);

        return new PageableDTO<>(stationPage, stationMapper.stationDTOListFromStationList(stationPage.getContent()));
    }

    public StationDTO findOne(Long id) {
        Optional<Station> stationOptional = stationRepository.findById(id);
        return stationMapper.stationDTOFromStation(stationOptional.orElseThrow(EntityNotFoundException::new));
    }

    public StationDTO create(StationDTO stationDTO){
        Station station = stationMapper.stationFromStationDTO(stationDTO);
        Company company = getCompanyFromDTO(stationDTO.getCompanyDTO());
        station.setCompany(company);
        station = stationRepository.save(station);
        return stationMapper.stationDTOFromStation(station);
    }

    public StationDTO update(Long id, StationDTO stationDTO){
        Station station = stationRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        station = stationMapper.getUpdateStationFromStationDTO(stationDTO, station);
        Company company = getCompanyFromDTO(stationDTO.getCompanyDTO());
        station.setCompany(company);
        station = stationRepository.save(station);
        return stationMapper.stationDTOFromStation(station);
    }

    public ValueResponse<Boolean> delete(Long id){
        if (!stationRepository.existsById(id)){
            throw new EntityNotFoundException();
        }
        stationRepository.deleteById(id);
        return new ValueResponse<>();
    }

    private Company getCompanyFromDTO(CompanyDTO dto){
        if(dto == null || dto.getId() == null){
            throw new InvalidParametersException("Company could not be null");
        }
        return companyRepository.findById(dto.getId())
                .orElseThrow(() -> new EntityNotFoundException("Company with id " + dto.getId() + " not found"));
    }

}

package com.pocket.repository;

import com.pocket.data.Station;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface StationRepository extends JpaRepository<Station, Long> {

    @Query(value = "SELECT *, SQRT(" +
            "    POW(69.1 * (latitude - :startlat), 2) +" +
            "    POW(69.1 * (:startlng - longitude) * COS(latitude / 57.3), 2)) AS distance " +
            "FROM station HAVING distance < :radius ORDER BY distance desc", nativeQuery = true)
    Page<Station> findAllWithinRadius(Integer radius, Double startlat, Double startlng, Pageable pageable);

    @Query(value = "SELECT * from station where company_id in :companyIds " , nativeQuery = true)
    Page<Station> findAllByCompanyId(Set<Long> companyIds, Pageable pageable);

}

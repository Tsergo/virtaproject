CREATE TABLE IF NOT EXISTS company
(
	id                          bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name                        VARCHAR(64),
    parent_company_id           bigint(20),
    created_at                  TIMESTAMP DEFAULT now(),
    updated_at                  TIMESTAMP DEFAULT now(),
    CONSTRAINT fk_parent_company_id
                                FOREIGN KEY(parent_company_id) REFERENCES company (id)
                                ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS station
(
    id                      bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name                    VARCHAR(64),
    latitude                double,
    longitude               double,
    created_at              TIMESTAMP DEFAULT now(),
    updated_at              TIMESTAMP DEFAULT now(),
    company_id              bigint(20),
    CONSTRAINT fk_company_id
                            FOREIGN KEY(company_id) REFERENCES company (id)
                            ON DELETE CASCADE ON UPDATE CASCADE
);



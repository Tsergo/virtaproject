package com.pocket.service;

import com.pocket.data.Company;
import com.pocket.dto.CompanyDTO;
import com.pocket.exceptions.EntityNotFoundException;
import com.pocket.mapper.CompanyMapper;
import com.pocket.repository.CompanyRepository;
import com.pocket.web.PageableDTO;
import com.pocket.web.ValueResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompanyService {

    private final CompanyMapper companyMapper;

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository, CompanyMapper companyMapper){
        this.companyRepository = companyRepository;
        this.companyMapper = companyMapper;
    }

    public PageableDTO<CompanyDTO> findAll(Pageable pageable){
        Page<Company> companyPage = companyRepository.findAll(pageable);
        return new PageableDTO<>(companyPage, companyMapper.companyDTOListFromCompanyList(companyPage.getContent()));
    }

    public CompanyDTO findOne(Long id) {
        Optional<Company> companyOptional = companyRepository.findById(id);
        return companyMapper.companyDTOFromCompany(companyOptional.orElseThrow(EntityNotFoundException::new));
    }

    public CompanyDTO create(CompanyDTO companyDTO){
        Company company = companyMapper.companyFromCompanyDTO(companyDTO);
        setParentCompany(company, companyDTO.getParentId());
        company = companyRepository.save(company);
        return companyMapper.companyDTOFromCompany(company);
    }

    public CompanyDTO update(Long id, CompanyDTO companyDTO){
        Company company = companyRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        setParentCompany(company, companyDTO.getParentId());
        company = companyMapper.getUpdateCompanyFromCompanyDTO(companyDTO, company);
        company = companyRepository.save(company);
        return companyMapper.companyDTOFromCompany(company);
    }

    public ValueResponse<Boolean> delete(Long id){
        if (!companyRepository.existsById(id)){
            throw new EntityNotFoundException();
        }
        companyRepository.deleteById(id);
        return new ValueResponse<>(true);
    }

    private void setParentCompany(Company company, Long id){
        if (id != null) {
            Company parentCompany = companyRepository.findById(id)
                    .orElseThrow(()-> new EntityNotFoundException("Company with the "  + id + " parentId not found"));
            company.setParent(parentCompany);
        }
    }
}

package com.pocket.web;

import org.springframework.data.domain.Page;

import java.util.List;

public class PageableDTO<T> {

	private List<T> items;
	private int totalPages;
	private long totalItems;
	private int page;
	private int size;

	public PageableDTO() {
	}

	public PageableDTO(List<T> items, int totalPages, long totalItems, int page, int size) {
		this.items = items;
		this.totalPages = totalPages;
		this.totalItems = totalItems;
		this.page = page;
		this.size = size;
	}

	public PageableDTO(Page page, List<T> items) {
		this(items, page.getTotalPages(), page.getTotalElements(), page.getNumber(), page.getSize());
	}

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public long getTotalItems() {
		return totalItems;
	}

	public void setTotalItems(long totalItems) {
		this.totalItems = totalItems;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
